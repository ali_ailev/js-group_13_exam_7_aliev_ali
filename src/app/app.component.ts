import {Component, Input} from '@angular/core';
import {Order} from "./shared/order.model";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  @Input() order: Order[] = [];


  getOrder(order:Order) {
    let flag = 0;
    console.log(order);
    for (let i = 0 ; i < this.order.length ; i++) {
      if(this.order[i].name === order.name) {
        this.order[i].amount++;
        this.order[i].price += this.order[i].price;
        flag = 1;
      }
    }
    if (flag === 0) {
      this.order.push(order);
    }
  }
}
