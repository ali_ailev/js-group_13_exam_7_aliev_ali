import {Component, Input} from '@angular/core';
import {Order} from "../shared/order.model";

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.css']
})
export class OrderDetailsComponent {
  @Input() order: Order[] = [];

  getOrderStatus() {
    return this.order.length !== 0;
  }

  getName(word:string) {
    let needWord:string = '';
    for (let i = 0 ; i < word.length ; i++) {
      if (word[i] === ' '){
        break;
      }
      needWord += word[i];
    }
    return needWord;
  }

  deleteOrder(event: Event) {
    event.preventDefault();
    const target = <HTMLFormElement> event.target;
    console.log(target.innerText);
    for (let i = 0 ; i < this.order.length ; i++) {
      if (this.getName(target.innerText) === this.order[i].name) {
        this.order.splice(i,1);
      }
    }
  }

  deleteAllOrder() {
    for (let i = 0 ; i < this.order.length; i++) {
      this.order.splice(i,);
    }
  }

  getSum() {
    let totalSum = 0;
    for (let i = 0 ; i < this.order.length ; i++) {
      totalSum += this.order[i].price;
    }
    return totalSum;
  }
}
