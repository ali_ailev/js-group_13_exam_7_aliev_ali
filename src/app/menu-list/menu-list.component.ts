import {Component, EventEmitter, Output} from '@angular/core';
import {Order} from "../shared/order.model";

@Component({
  selector: 'app-menu-list',
  templateUrl: './menu-list.component.html',
  styleUrls: ['./menu-list.component.css']
})
export class MenuListComponent {
  dishes = [
    {image: 'https://media-cdn.tripadvisor.com/media/photo-s/14/b1/f3/42/lagman-boso.jpg',name: 'Lagman', price: 140},
    {image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/6/60/%D0%9E%D1%80%D0%BE%D0%BC%D0%BE001.jpg/220px-%D0%9E%D1%80%D0%BE%D0%BC%D0%BE001.jpg',name: 'Oromo', price: 250},
    {image: 'https://www.gastronom.ru/binfiles/images/20161118/b0a987b6.jpg',name: 'Samsi', price: 50},
    {image: 'https://cdn.profile.ru/wp-content/uploads/2020/01/shutterstock_139065488.jpg',name: 'Tea', price: 40},
    {image: 'https://primamedia.gcdn.co/f/big/1964/1963564.jpg?78cc9278f3af848ebdb7bb28c8a94039',name: 'Water', price: 30},
    {image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/4/45/A_small_cup_of_coffee.JPG/274px-A_small_cup_of_coffee.JPG',name: 'Coffee', price: 50},
  ];
  @Output() madeOrder= new EventEmitter<Order>()
  order: Order[] = [];

  addNewOrder (order: Order) {
    this.madeOrder.emit(order)
  }


}
